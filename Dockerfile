FROM php:7.4-apache

COPY /jobs-php /var/www/html

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

WORKDIR /app/jobs-php

EXPOSE 80